﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services.Services.Contract;

namespace Application.Controllers
{
    [ApiController]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiVersion("1.0")]
    public class MovimentacaoFinanceiraController : BaseController
    {
        public MovimentacaoFinanceiraController()
        {
        }

        [HttpGet(Name = "GetDateTimeNow")]
        public IActionResult GetDateTimeNow([FromServices] IMovimentacaoFinanceiraService service)
        {
            return Response(service.GetDateTimeNow());
        }
    }
}
