using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.AspNetCore.Mvc;
using Infra.CrossCutting;


var builder = WebApplication.CreateBuilder(args);

builder.Services.AddHttpClient();
builder.Services.AddControllers();
builder.Services.AddHealthChecks();

builder.Services
    .AddHealthChecksUI()
    .AddInMemoryStorage();

builder.Services.Configure<ForwardedHeadersOptions>(c =>
    {
        c.ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
    });

#region ApiVersion
builder.Services.AddApiVersioning(c =>
    {
        c.AssumeDefaultVersionWhenUnspecified = true;
        c.ReportApiVersions = true;
        c.DefaultApiVersion = new ApiVersion(1, 0);
        c.ApiVersionReader = new UrlSegmentApiVersionReader();
    });

builder.Services.AddVersionedApiExplorer(c =>
    {
        c.GroupNameFormat = "'v'VVV";
        //versioning by url segment
        c.SubstituteApiVersionInUrl = true;
    });
#endregion

#region CORS
builder.Services.AddCors(c =>
    {
        c.AddPolicy("CorsPolicy-public", builder => builder
            .AllowAnyOrigin()
            .AllowAnyMethod()
            .AllowAnyHeader()
            .Build());
    });
#endregion

#region MVC and JSON options
builder.Services.AddMvc(c => c.EnableEndpointRouting = false);
#endregion

// Add services to the container.
NativeInjectorBootStrapper.RegisterServices(builder.Services);

builder.Services.AddControllers();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();

#region Swagger API
builder.Services.AddSwaggerGen(c =>
    {
        c.SwaggerDoc("v1", new OpenApiInfo { Title = "webapp-api", Version = "v1" });
        c.CustomSchemaIds(x => x.FullName);
    });
#endregion

builder.Services.AddMvc(c => c.EnableEndpointRouting = false)
                .AddNewtonsoftJson(c => { c.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore; });

builder.Services.Configure<FormOptions>(c =>
{
    c.ValueLengthLimit = int.MaxValue;
    c.MultipartBodyLengthLimit = int.MaxValue;
    c.MemoryBufferThreshold = int.MaxValue;
});

var app = builder.Build();

app.UseStaticFiles();
app.UseSwagger();
app.UseSwaggerUI(c =>
    {
        c.SwaggerEndpoint("/swagger/v1/swagger.json", "Api 1.0");
        c.ConfigObject.AdditionalItems.Add("syntaxHighlight", false);
        c.DisplayOperationId();
        c.DisplayRequestDuration();
    });

app.UseForwardedHeaders();
app.UseCors("CorsPolicy-public");
app.UseHttpsRedirection();

app.UseAuthorization();
app.MapControllers();

app.UseHealthChecks("/healthcheck", new HealthCheckOptions
    {
        Predicate = _ => true,
        ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
    })
    .UseHealthChecksUI(setup =>
    {
        setup.AddCustomStylesheet(@$"wwwroot/css/healthChecks.css");
        setup.UIPath = "/dashboard";
    });

app.Run();
