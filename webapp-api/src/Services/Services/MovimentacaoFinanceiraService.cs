﻿using Services.Services.Contract;

namespace Services.Services
{
    public class MovimentacaoFinanceiraService : IMovimentacaoFinanceiraService
    {
        public DateTime GetDateTimeNow() => DateTime.Now;
    }
}
