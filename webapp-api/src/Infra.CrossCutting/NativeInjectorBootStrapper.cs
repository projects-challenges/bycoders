﻿using Microsoft.Extensions.DependencyInjection;
using Services.Services;
using Services.Services.Contract;


namespace Infra.CrossCutting
{
    public class NativeInjectorBootStrapper
    {
        public static void RegisterServices(IServiceCollection services)
        {
            #region Services
            services.AddTransient<IMovimentacaoFinanceiraService, MovimentacaoFinanceiraService>();
            #endregion
        }
    }
}
