#See https://aka.ms/customizecontainer to learn how to customize your debug container and how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app

# Configure timezone
RUN unlink /etc/localtime
RUN ln -s /usr/share/zoneinfo/Brazil/East /etc/localtime

RUN apt-get update \
    && apt-get install -y telnet \
    && apt-get install -y inetutils-traceroute \
    && apt-get install -y traceroute \
    && apt-get install -y inetutils-ping \
    && apt-get install -y iputils-ping \
    && apt-get install -y tcpdump \
    && apt-get install -y curl \
    && apt-get install -y netcat \
    && apt-get install -y net-tools \
    && apt-get install -y dnsutils \
    && apt install -y --no-install-recommends openssh-server \
    && mkdir -p /run/sshd \
    && echo "root:Docker!" | chpasswd

# Copy the sshd_config file to the /etc/ssh/ directory
COPY sshd_config /etc/ssh/sshd_config

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY src/Application/Application.csproj ./src/Application/
RUN dotnet restore ./src/Application/Application.csproj
COPY src ./
RUN dotnet build /src/Application/Application.csproj -c Release -o /app/build

FROM build AS publish
RUN dotnet publish /src/Application/Application.csproj -c Release -o /app/publish /p:UseAppHost=false


FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .


# Open port 2222 for SSH access
EXPOSE 80
EXPOSE 2222 80
EXPOSE 443

ENTRYPOINT ["/bin/bash", "-c", "/usr/sbin/sshd && dotnet Application.dll"]
